# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.23.0](https://gitlab.com/france-identite/rendez-vous-mairie/compare/2.22.0...2.23.0) (2023-06-30)


### Features

* add some unit test ([b3e14a5](https://gitlab.com/france-identite/rendez-vous-mairie/commit/b3e14a5eb0e50853d8afbe9f6b297ea12aaacee1))
* Rajouter l'éditeur Mushroom ([48158b3](https://gitlab.com/france-identite/rendez-vous-mairie/commit/48158b37e7f693b19725ab023da936e5d0b42734))
* Rajouter la mairie de Sens ([5441a27](https://gitlab.com/france-identite/rendez-vous-mairie/commit/5441a27fad6ca232728ddc3e1d5464a54f27745a))


### Bug Fixes

* slot schedule for Bussy && Creney-près Troyes ([dc324ec](https://gitlab.com/france-identite/rendez-vous-mairie/commit/dc324ec781647706df1c6f963444e6dc050ac42f))
* sort-by-distance-and-other-issues ([05dde68](https://gitlab.com/france-identite/rendez-vous-mairie/commit/05dde68b806c460ae42f5ae4446c695dfd919b37))
