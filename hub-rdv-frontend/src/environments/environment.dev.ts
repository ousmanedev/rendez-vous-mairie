export const environment = {
  production: false,
  api_url: 'https://ppd.api.rendezvouspasseport.ants.gouv.fr/api',
  wss_url: 'wss://ppd.api.rendezvouspasseport.ants.gouv.fr/api',
  address_gouv_api: 'https://api-adresse.data.gouv.fr/reverse/',
  address_gouv_api_search: 'https://api-adresse.data.gouv.fr/search/',
  recaptcha: {
    site_key: '6Leu1eYhAAAAANv8x5qG9sQnbiNmyX2w03GCutK7',
  },
  googleSiteVerification: 'XXX',
};
