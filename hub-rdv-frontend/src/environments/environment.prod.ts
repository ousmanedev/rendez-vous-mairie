export const environment = {
  production: true,
  api_url: 'https://api.rendezvouspasseport.ants.gouv.fr/api',
  wss_url: 'wss://api.rendezvouspasseport.ants.gouv.fr/api',
  address_gouv_api: 'https://api-adresse.data.gouv.fr/reverse/',
  address_gouv_api_search: 'https://api-adresse.data.gouv.fr/search/',
  recaptcha: {
    site_key: '6Leu1eYhAAAAANv8x5qG9sQnbiNmyX2w03GCutK7',
  },
  googleSiteVerification: '4pQy-f6UJKDM_LEiLaiHshub9lo4ZmnGm3FUNprY0aA',
};
