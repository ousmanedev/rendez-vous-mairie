// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  api_url: 'http://localhost:8081/api',
  wss_url: 'ws://localhost:8081/api',
  address_gouv_api: 'https://api-adresse.data.gouv.fr/reverse/',
  address_gouv_api_search: 'https://api-adresse.data.gouv.fr/search/',
  recaptcha: {
    site_key: '6Leu1eYhAAAAANv8x5qG9sQnbiNmyX2w03GCutK7',
  },
  googleSiteVerification: 'YYY',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
