import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PredemandeComponent } from './predemande/predemande.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PredemandeRoutingModule } from './predemande-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SharedModule } from '@shared/shared.module';
import { SearchPredemandeComponent } from './search-predemande/search-predemande.component';
import { ListPredemandeComponent } from './list-predemande/list-predemande.component';

@NgModule({
  declarations: [PredemandeComponent, SearchPredemandeComponent, ListPredemandeComponent],
  imports: [PredemandeRoutingModule, FormsModule, ReactiveFormsModule, CommonModule, NgxSpinnerModule, SharedModule],
})
export class PredemandeModule {}
