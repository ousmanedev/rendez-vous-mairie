import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PredemandeComponent } from './predemande/predemande.component';

const routes: Routes = [{ path: 'NumeroPredemande', component: PredemandeComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PredemandeRoutingModule {}
