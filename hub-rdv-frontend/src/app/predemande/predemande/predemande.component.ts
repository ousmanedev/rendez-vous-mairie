import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { PredemandeService } from '@shared/services/predemande.service';
import { ReCaptchaV3Service } from 'ng-recaptcha';
import { DeviceStateService } from '@shared/services/device-state.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { SEOService } from '@shared/services/seoservice.service';
import { Observable } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'rdv-predemande',
  templateUrl: './predemande.component.html',
  styleUrls: ['./predemande.component.scss'],
})
/**
 * Class representing the pré-demande module component.
 * @extends PredemandeComponent
 */
export class PredemandeComponent implements OnInit {
  /**
   *  End of serach indicator
   * @type {boolean}
   */
  endOfSearch?: boolean;
  @Input() isDesktop: boolean;

  /**
   * Pré-demande IDs input
   * @type {string}
   */
  predemandes: string = '';
  /**
   * Pré-demande IDs input
   * @type {string}
   */
  ValidateError: boolean = false;
  /**
   * Pré-demande IDs validator to validate form ?
   * @type {boolean}
   */
  FormatError: boolean = false;
  /**
   * Is there a Format Error in Pré-demande IDs on key press ?
   * @type {boolean}
   */
  errorMessage: any;

  /**
   * Is scrolled ?
   * @type {boolean}
   */
  scrolled: boolean = false;
  /**
   * A map-like object that contains the pré-demande slots per pré-demande ID .
   *
   * @type {Object.<string, Array<any>>}
   */
  predemande_results: Map<string, Array<any>> = new Map();
  /**
   * A Regular Expression to check if the typed pré-demande IDs are valid or not on key press
   *
   * @type {RegExp}
   */
  RegexFormat: RegExp = /^([a-zA-Z0-9]{10}[,;:\-/.\s])*[a-zA-Z0-9]{0,10}$/;
  /**
   * A Regular Expression to check if the typed pré-demande IDs are valid or not when search button is clicked
   *
   * @type {RegExp}
   */
  RegexFormatSearch: RegExp = /^([a-zA-Z0-9]{10}[,;:\-/.\s])*[a-zA-Z0-9]{10}$/;

  /**
   * @constructor
   * @param {PredemandeService} predemandeService - The router service.
   * @param {ReCaptchaV3Service} recaptchaV3Service - The Google recaptcha V3 Service.
   * @param {DeviceStateService} deviceState - The Device State service.
   * @param {ChangeDetectorRef} cdRef - The Change Ref Detector service.
   * @param {NgxSpinnerService} spinner - The NgxSpinnerService service.
   * @param {ActivatedRoute} route - The Routing activated service.
   * @param {location} location - The  window location service.
   */
  constructor(
    private predemandeService: PredemandeService,
    private recaptchaV3Service: ReCaptchaV3Service,
    private deviceState: DeviceStateService,
    private cdRef: ChangeDetectorRef,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private location: Location,
    private seoservice: SEOService,
    private title: Title
  ) {
    this.isDesktop = !this.deviceState.isMobileResolution();
  }

  /**
   * Initialization tasks
   * Check if there are query parameters, if so, call the search function
   */
  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.title.setTitle('ANTS - Retrouvez vos rendez-vous en mairie(s)');
    this.seoservice.updateCanonicalUrl('https://rendezvouspasseport.ants.gouv.fr/predemande/NumeroPredemande');

    const routeFragment: Observable<any> = this.route.fragment;
    if (routeFragment)
      routeFragment.subscribe((fragment) => {
        const searchParams = new URLSearchParams(fragment);
        if (fragment) {
          let ids = searchParams.get('ids');
          if (ids) this.onSearch(ids);
        }
      });

    this.route.queryParams.subscribe((params) => {
      let ids = params['ids'];
      if (ids) this.onSearch(ids);
    });
  }

  /**
   * Search for slots by its pré-demande id(s)
   * @param {any} searchCriteria : list of pré-demande ids
   */
  onSearch(searchCriteria: any) {
    this.spinner.show();
    this.endOfSearch = false;
    let predemandes: string;
    if (typeof searchCriteria === 'object') predemandes = searchCriteria.predemandes;
    else predemandes = searchCriteria;
    this.location.go('predemande/NumeroPredemande#ids=' + predemandes);

    let i = 0;
    this.recaptchaV3Service.execute('myAction').subscribe({
      next: (token: any) => {
        const application_ids = predemandes.split(/[/.,;:\s]/);
        this.predemandeService.get(token, application_ids).subscribe(
          (res: any) => {
            this.errorMessage = {};
            this.predemande_results = new Map(Object.entries(res));

            this.predemande_results.forEach((meetings: any, key) => {
              meetings = meetings
                .filter((row: any) => {
                  let d;
                  if (row['datetime'].slice(-1) === 'Z') d = new Date(row['datetime'].replace('Z', ''));
                  else d = new Date(row['datetime']);
                  row.hour = d.toString();
                  return row;
                })
                .reduce((accumulator: any, current: any) => {
                  if (
                    !accumulator.find((item: any) => item.meeting_point === current.meeting_point && item.datetime === current.datetime)
                  ) {
                    if (new Date(current.datetime) >= new Date()) accumulator.push(current);
                  }
                  return accumulator;
                }, [])
                .sort((a: any, b: any) => {
                  return <any>new Date(a.datetime) - <any>new Date(b.datetime);
                });

              this.predemande_results.set(key, meetings);
            });

            this.endOfSearch = true;
            this.scroll();
          },
          (error) => {
            this.endOfSearch = true;
            if (error.error && error.error.detail && error.error.detail.includes('Low score')) {
              this.errorMessage = {
                description: 'Captcha invalide',
              };
            } else {
              this.errorMessage = {
                title: 'Connexion',
                description: 'Connexion au serveur impossible. Veuillez vérifier votre connexion internet ou réessayer plus tard.',
              };
            }
          }
        );
      },
      error: (e: any) => {
        console.error(`Recaptcha v3 error:`, e);
      },
    });
  }

  /**
   * id(s) format validator on search click
   */
  RegexSearchValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (control.value) {
        if (!control.value.toString().match(this.RegexFormat)) this.FormatError = true;
        else this.FormatError = false;

        if (!control.value.toString().match(this.RegexFormatSearch)) {
          this.ValidateError = true;
          return { valide: true };
        }
      }
      this.ValidateError = false;
      this.FormatError = false;
      return null;
    };
  }

  /**
   * Scroll down to the displayed results
   */
  scroll() {
    this.cdRef.detectChanges();
    if (this.predemande_results?.size !== 0 && this.endOfSearch) {
      let scrolltoelement = window.document.getElementById('target')! as HTMLInputElement;
      if (scrolltoelement) {
        scrolltoelement.scrollIntoView({
          behavior: 'smooth',
          block: 'start',
          inline: 'nearest',
        });
        this.scrolled = true;
      }
    } else
      setTimeout(() => {
        this.scroll();
      }, 500);
  }

  /**
   * Sort slots by date for each predemende id
   * @param {Array<string>} list : list of slots for a given predemende id
   */
  sortDates(list: Array<string>) {
    return list.sort((a, b) => {
      return <any>new Date(a) - <any>new Date(b);
    });
  }

  rowIsUnique(a: any, b: any): boolean {
    //return !(a._meeting_point === b._meeting_point && a.dateTime === b.dateTime);
    return !(a.meeting_point === b.meeting_point && a.datetime === b.datetime);
  }
}
