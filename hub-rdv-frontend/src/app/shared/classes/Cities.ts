export class Cities {
  _cancel_url?: string;
  _datetime?: string;
  _management_url?: string;
  _meeting_point?: string;
  _meeting_point_id?: number;
  _hour?: string;

  constructor() {}

  get cancel_url(): string | undefined {
    return this._cancel_url;
  }
  set cancel_url(cancel_url: string | undefined) {
    this._cancel_url = cancel_url;
  }

  get management_url(): string | undefined {
    return this._management_url;
  }
  set management_url(management_url: string | undefined) {
    this._management_url = management_url;
  }

  get meeting_point(): string | undefined {
    return this._meeting_point;
  }
  set meeting_point(meeting_point: string | undefined) {
    this._meeting_point = meeting_point;
  }

  get meeting_point_id(): number | undefined {
    return this._meeting_point_id;
  }
  set meeting_point_id(meeting_point_id: number | undefined) {
    this._meeting_point_id = meeting_point_id;
  }

  get datetime(): string | undefined {
    return this._datetime;
  }
  set datetime(datetime: string | undefined) {
    this._datetime = datetime;
  }

  get hour(): string | undefined {
    return this._hour;
  }
  set hour(hour: string | undefined) {
    this._hour = hour;
  }
}
