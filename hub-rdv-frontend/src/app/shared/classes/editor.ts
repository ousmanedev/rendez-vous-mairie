export interface Editor {
  slug: string;
  name: string;
  api_url: string;
  status: Boolean;
  api_down_datetime: Date;
  api_up_datetime: Date;
  _test_mode: Boolean;
}
