export interface Municipality {
  id: string;
  name: string;
  longitude: number;
  latitude: number;
  public_entry_address?: string;
  zip_code?: string;
  city_name?: string;
  decoded_city_name?: string;
  website?: string;
  city_logo?: string;
  available_slots?: Array<Creneau>;
  distance_km: number;
  phone_number?: number;
  department_name?: string;
  canonized_name?: string;
  canonized_department_name?: string;
}

export interface Creneau {
  callback_url: string;
  datetime: string;
}
