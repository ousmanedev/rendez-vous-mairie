import { DateUtils } from '@shared/utils/date';

export class SearchCriteria {
  longitude?: number;
  latitude?: number;
  start_date?: string;
  end_date?: string;
  radius_km?: number;
  address?: string;
  reason?: string;
  documents_number?: number;
  department_code?: string;
  reCAPTCHA_token?: string;
  smart_search?: boolean;

  constructor() {}
}

export function argsAsList(search: SearchCriteria): Array<string> {
  return [
    DateUtils.getDateFormat(search.start_date),
    DateUtils.getDateFormat(search.end_date),
    valueOf(search.address),
    valueOf(search.radius_km),
  ];
}

export function valueOf(value: string | number | undefined): string {
  return value == undefined ? '' : value.toString();
}
