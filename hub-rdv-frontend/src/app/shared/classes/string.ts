export interface IString {
  format(str: string, ...replacements: string[]): string;
}

export class String implements IString {
  constructor() {}

  format(str: string, ...replacements: string[]): string {
    return str.replace(/{(\d+)}/g, function (match, number) {
      return typeof replacements[number] != 'undefined' ? replacements[number] : match;
    });
  }
}
