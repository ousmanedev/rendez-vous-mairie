import { SafeUrl } from '@angular/platform-browser';

export class Slot {
  _id?: number;
  // change to municipality Object
  _municipalityId?: number;
  _municipality?: string;
  _date?: Date;
  _duration: number = 0;
  _distance: number = 0;
  // _url?: SafeUrl;
  _url?: string;
  _hour?: string;
  _logo?: SafeUrl;
  public_entry_address?: string;
  _phone_number?: string;
  _website?: string;
  _zipCode?: string;
  _name?: string;

  constructor() {}

  get name(): string | undefined {
    return this._name;
  }
  set cityName(cityName: string | undefined) {
    this._name = cityName;
  }

  get zipCode(): string | undefined {
    return this._zipCode;
  }
  set zipCode(zipCode: string | undefined) {
    this._zipCode = zipCode;
  }
  get id(): number | undefined {
    return this._id;
  }
  set id(id: number | undefined) {
    this._id = id;
  }

  get municipalityId(): number | undefined {
    return this._municipalityId;
  }
  set municipalityId(municipalityId: number | undefined) {
    this._municipalityId = municipalityId;
  }

  get municipality() {
    return this._municipality;
  }

  get phone_number() {
    return this._phone_number;
  }
  set municipality(municipality) {
    this._municipality = municipality;
  }

  get date(): Date | undefined {
    return this._date;
  }
  set date(date: Date | undefined) {
    this._date = date;
  }

  get duration() {
    return this._duration;
  }
  set duration(duration) {
    this._duration = duration;
  }

  get distance() {
    return this._distance;
  }
  set distance(distance) {
    this._distance = distance;
  }

  get url() {
    return this._url;
  }
  set url(url) {
    this._url = url;
  }

  get website() {
    return this._url;
  }
  set website(url) {
    this._website = url;
  }

  get hour() {
    return this._hour;
  }
  set hour(hour) {
    this._hour = hour;
  }

  get logo(): SafeUrl | undefined {
    return this._logo;
  }
  set logo(_logo: SafeUrl | undefined) {
    this._logo = _logo;
  }

  get publicEntryAddress(): string | undefined {
    return this.public_entry_address;
  }
  set publicEntryAddress(_publicEntryAddress: string | undefined) {
    this.public_entry_address = _publicEntryAddress;
  }
}
