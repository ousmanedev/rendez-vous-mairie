import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalRedirectionComponent } from './modal-redirection.component';

describe('ModalRedirectionComponent', () => {
  let component: ModalRedirectionComponent;
  let fixture: ComponentFixture<ModalRedirectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ModalRedirectionComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ModalRedirectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
