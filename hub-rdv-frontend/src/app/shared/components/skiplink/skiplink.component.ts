import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'rdv-skiplink',
  templateUrl: './skiplink.component.html',
  styleUrls: ['./skiplink.component.scss'],
})
export class SkiplinkComponent {
  constructor(private router: Router) {}

  createHref(link: string): string {
    return this.removeAnchor(this.router.url).concat(link);
  }
  /**
   * Supprime l'ancre précédente afin de pouvoir se servir tout de même des liens d'évitements
   */
  removeAnchor(url: string): string {
    const anchorIndex = url.lastIndexOf('#');
    if (anchorIndex !== -1) {
      const anchor = url.slice(anchorIndex + 1);
      if (/^[a-zA-Z-]+$/.test(anchor)) {
        return url.slice(0, anchorIndex);
      }
    }
    return url;
  }
}
