import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalUserDeniedComponent } from './modal-user-denied.component';

describe('ModalUserDeniedComponent', () => {
  let component: ModalUserDeniedComponent;
  let fixture: ComponentFixture<ModalUserDeniedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ModalUserDeniedComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ModalUserDeniedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
