import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'rdv-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  appName = 'Agence nationale des titres sécurisés';
  appDescription = "Plateforme de rendez-vous pour la délivrance de passeports et de cartes d'identité";

  constructor(private router: Router) {}
  ngOnInit(): void {}

  searchByTopic1(): boolean {
    return (
      this.router.url == '/villes' ||
      this.router.url == '/departements' ||
      this.router.url.includes('/ville/') ||
      this.router.url.includes('/departement/')
    );
  }
  searchByTopic2(): boolean {
    return (
      this.router.url == '/informations' ||
      this.router.url == '/rejoindre-la-demarche' ||
      this.router.url == '/pre-demande-en-ligne' ||
      this.router.url == '/deterritorialisation'
    );
  }
}
