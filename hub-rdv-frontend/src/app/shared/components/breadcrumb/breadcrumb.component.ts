import { Component, Input, OnInit } from '@angular/core';
import { BreadcrumbItem } from '@shared/classes/BreadcrumbItem';

@Component({
  selector: 'rdv-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
})
export class BreadcrumbComponent implements OnInit {
  @Input() items?: Array<BreadcrumbItem> | undefined;
  constructor() {}

  ngOnInit(): void {}
}
