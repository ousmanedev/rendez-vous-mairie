import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeolocalisationUndefinedComponent } from './geolocalisation-undefined.component';

describe('GeolocalisationUndefinedComponent', () => {
  let component: GeolocalisationUndefinedComponent;
  let fixture: ComponentFixture<GeolocalisationUndefinedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GeolocalisationUndefinedComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(GeolocalisationUndefinedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
