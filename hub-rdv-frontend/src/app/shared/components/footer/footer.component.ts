import { Component, OnInit } from '@angular/core';
// import { version } from '../../../../package.json';
declare const require: (path: string) => any;
@Component({
  selector: 'rdv-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  APP_VERSION: any;
  DATE_VERSION: any;
  constructor() {}

  ngOnInit(): void {
    this.APP_VERSION = require('../../../../../package.json').version;
    this.DATE_VERSION = require('../../../../../version-date.json').dateRelease;
  }
}
