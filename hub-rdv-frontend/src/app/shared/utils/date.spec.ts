import { TestBed } from '@angular/core/testing';
import moment from 'moment';

import { DateUtils } from './date';

describe('DateUtils', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [],
    });
  });

  it('should return string date if getDateFormat is called', () => {
    const result = DateUtils.getDateFormat('2022-12-12');
    expect(result).toBeInstanceOf(String);
  });

  it('should return string date if getDate is called', () => {
    const result = DateUtils.getDate('2022-12-12');
    expect(result).toBeInstanceOf(String);
  });

  it('should return string date if getDefaultStartDate is called and same date as today', () => {
    const result = DateUtils.getDefaultStartDate();
    expect(result).toBeInstanceOf(String);
    const resultMoment = moment(result);
    expect(resultMoment.date()).toEqual(new Date().getDate());
  });

  it('should return string date if getDefaultEndDate is called and today + 3M', () => {
    const result = DateUtils.getDefaultEndDate(new Date().toDateString());
    expect(result).toBeInstanceOf(String);
    const resultMoment = moment(result);
    expect(resultMoment.day()).toEqual(moment(new Date()).add(3, 'M').day());
  });

  it('should return string date if getEndDateMax is called and today + 3M', () => {
    const result = DateUtils.getEndDateMax(new Date().toDateString());
    expect(result).toBeInstanceOf(String);
    const resultMoment = moment(result);
    expect(resultMoment.day()).toEqual(moment(new Date()).add(3, 'M').day());
  });
});
