import { Municipality } from '@shared/classes/municipality';

export class Utils {
  static cannonize(input: string | undefined): string {
    if (input) {
      return input
        .toLowerCase()
        .replace(/ +/g, '') //suppression espace
        .replaceAll(/-/g, '') //suppresssion des tirets
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '');
    }
    return '';
  }
  static __getLocaleTimeFormat__(d: any) {
    let date = new Date(d);
    var minutes = ('0' + date.getMinutes()).slice(-2);
    let appointement = date.getHours() + 'h' + minutes;
    return appointement;
  }
}
