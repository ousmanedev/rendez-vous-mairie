import { Municipality } from '@shared/classes/municipality';
import { SearchCriteria } from '@shared/classes/search-criteria';
import { Slot } from '@shared/classes/slot';
import { multicast, range } from 'rxjs';

export abstract class Appointment {
  static groupMunicipaltiesByDistance(municipalities: Array<Slot>): Map<string, Array<Slot>> {
    let OfflineMunicipalities: Map<string, Array<Slot>> = new Map();
    let ranges = 20 / 5;
    for (let i = 0; i < ranges; i++) OfflineMunicipalities.set(((i + 1) * 5).toString(), new Array());
    municipalities.forEach((municipality: Slot) => {
      municipality.publicEntryAddress = municipality['public_entry_address'];
      for (let i = 0; i < ranges; i++) {
        if (municipality.distance < (i + 1) * 5 && municipality.distance >= (i + 1) * 5 - 5) {
          OfflineMunicipalities.get(((i + 1) * 5).toString())?.push(municipality);
        }
      }
    });

    return OfflineMunicipalities;
  }
}
