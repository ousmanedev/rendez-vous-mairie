import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { ValidationErrors } from '@angular/forms';
import { date } from '@form-validation/validator-date';
import { DateUtils } from '@shared/utils/date';
import moment from 'moment';

@Injectable({
  providedIn: 'root',
})
export class DateValidator {
  constructor() {}

  public static dateRangeValidator(control: AbstractControl): ValidationErrors | null {
    if (control.get('start_date')?.value > control.get('end_date')?.value) return { invalidDateRange: true };
    return null;
  }

  public static dateFormatValidator(control: AbstractControl): ValidationErrors | null {
    let resStartDate = date().validate({
      value: control.get('start_date')?.value,
      options: {
        format: 'YYYY-MM-DD',
      },
    });
    let resEndDate = date().validate({
      value: control.get('end_date')?.value,
      options: {
        format: 'YYYY-MM-DD',
      },
    });
    if (!resStartDate.valid) return { invalidFormatStartDate: true };
    if (!resEndDate.valid) return { invalidFormatEndDate: true };
    return null;
  }

  public static startDateRangeValidator(control: AbstractControl): ValidationErrors | null {
    if (control.get('start_date')?.value < DateUtils.getDefaultStartDate()) return { invalidStartDate: true };
    return null;
  }

  public static endDateRangeValidator(control: AbstractControl): ValidationErrors | null {
    const limitDate = moment(new Date(control.get('start_date')?.value))
      .add(3, 'M')
      .format('YYYY-MM-DD');
    if (control.get('end_date')?.value <= limitDate) return { invalidEndDate: true };
    return null;
  }
}
