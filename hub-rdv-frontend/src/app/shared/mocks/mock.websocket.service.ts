import { EventEmitter } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { AnonymousSubject } from 'rxjs/internal/Subject';
import { WebsocketService } from '@shared/services/websocket.service';

let observable = new Observable((obs: Observer<MessageEvent>) => {
  return;
});
let observer = {
  next: (data: Object) => {
    console.log('sending data through websocket');
  },
  error: (err: any) => {
    throw new Error('Websocket error: ' + err);
  },
  complete: () => {
    console.log('Websocket complete!');
  },
};

export const fakeWebsocketService: Pick<WebsocketService, keyof WebsocketService> = {
  url: 'ws://mock-websocket.test/api/SlotsFromPositionStreaming',
  messageSubject: new AnonymousSubject<MessageEvent>(observer, observable),
  ws: undefined,
  isConnected: new EventEmitter<any>(),
  connect(): AnonymousSubject<MessageEvent> {
    this.messageSubject = new AnonymousSubject<MessageEvent>(observer, observable);
    return this.messageSubject;
  },
  isWsOpen() {
    return true;
  },
  checkWebsocketConnexion(): void {
    setInterval(() => {
      if (!this.isWsOpen()) {
        this.connect();
      }
    }, 5000);
  },
};
