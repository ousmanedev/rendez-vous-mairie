import { ReCaptchaV3Service } from 'ng-recaptcha';
import { OnExecuteData, OnExecuteErrorData } from 'ng-recaptcha/lib/recaptcha-v3.service';
import { Observable, Subject } from 'rxjs';

export const fakeRecaptchaV3Service: Pick<ReCaptchaV3Service, keyof ReCaptchaV3Service> = {
  onExecute: new Observable<OnExecuteData>(),
  onExecuteError: new Observable<OnExecuteErrorData>(),
  execute(action: string): Observable<string> {
    let subj = new Subject<string>();
    return subj.asObservable();
  },
};
