import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ModalUserDeniedComponent } from './components/modal-user-denied/modal-user-denied.component';
import { GeolocalisationUndefinedComponent } from './components/geolocalisation-undefined/geolocalisation-undefined.component';
import { ModalRedirectionComponent } from './components/modal-redirection/modal-redirection.component';
import { AnnouncementComponent } from './components/announcement/announcement.component';
import { BreadcrumbComponent } from '@shared/components/breadcrumb/breadcrumb.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SkiplinkComponent } from './components/skiplink/skiplink.component';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    NotFoundComponent,
    ModalUserDeniedComponent,
    GeolocalisationUndefinedComponent,
    ModalRedirectionComponent,
    AnnouncementComponent,
    BreadcrumbComponent,
    SkiplinkComponent,
  ],
  imports: [CommonModule, RouterModule],
  exports: [
    ModalUserDeniedComponent,
    GeolocalisationUndefinedComponent,
    ModalRedirectionComponent,
    AnnouncementComponent,
    BreadcrumbComponent,
    HeaderComponent,
    FooterComponent,
    SkiplinkComponent,
  ],
})
export class SharedModule {}
