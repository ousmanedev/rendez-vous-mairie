import { TestBed } from '@angular/core/testing';

import { SEOService } from './seoservice.service';

describe('SEOService', () => {
  let seoService: SEOService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    seoService = TestBed.inject(SEOService);
  });

  it('should be created', () => {
    expect(seoService).toBeTruthy();
  });
});
