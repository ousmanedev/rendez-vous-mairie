import { EventEmitter, Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { AnonymousSubject } from 'rxjs/internal/Subject';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class WebsocketService {
  url: string = environment.wss_url + '/SlotsFromPositionStreaming';
  public messageSubject?: AnonymousSubject<any>;
  ws?: WebSocket;
  isConnected: EventEmitter<any>;
  constructor(private router: Router) {
    this.connect();
    this.checkWebsocketConnexion();
    this.isConnected = new EventEmitter<any>();
  }

  public connect(): AnonymousSubject<MessageEvent> {
    this.messageSubject = this.create(this.url);
    return this.messageSubject;
  }

  private create(url: string): AnonymousSubject<MessageEvent> {
    try {
      let ws = new WebSocket(url);
      let observable = new Observable((obs: Observer<MessageEvent>) => {
        ws.onmessage = obs.next.bind(obs);
        ws.onerror = obs.error.bind(obs);
        ws.onclose = obs.complete.bind(obs);
        return ws.close.bind(ws);
      });
      let observer = {
        next: (data: Object) => {
          if (this.ws?.readyState === WebSocket.OPEN) {
            this.ws?.send(JSON.stringify(data));
          } else {
            this.messageSubject?.error('Websocket needs to reconnect');
          }
        },
        error: (err: any) => {
          throw new Error('Websocket error: ' + err);
        },
        complete: () => {
          console.log('Websocket complete!');
        },
      };
      this.ws = ws;
      return new AnonymousSubject<MessageEvent>(observer, observable);
    } catch (error) {
      throw new Error('Error while connecting to websocket : ' + error);
    }
  }

  isWsOpen() {
    return this.ws !== undefined ? this.ws.readyState === WebSocket.OPEN : false;
  }

  checkWebsocketConnexion(): void {
    setInterval(() => {
      this.isConnected.emit({ connected: this.isWsOpen() });
      if (!this.isWsOpen()) {
        this.connect();
      }
    }, 5000);
  }
}
