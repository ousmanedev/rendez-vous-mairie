export class NativeHtmlScripts {
  public static correctRecaptchaAccessibility() {
    let recaptchaResponse = document.getElementsByClassName('g-recaptcha-response');
    for (let i = 0; i < recaptchaResponse.length; i++) {
      recaptchaResponse[i].setAttribute('title', 'Recaptcha Response');
      recaptchaResponse[i].setAttribute('aria-label', 'Recaptcha Response');
    }

    let recaptchaBadge = document.getElementsByClassName('grecaptcha-badge');
    if (recaptchaBadge) {
      let recaptchaIframe = <HTMLElement>recaptchaBadge[0]?.parentNode?.lastChild;
      recaptchaIframe?.setAttribute('title', 'Recaptcha Iframe');
    }
  }
}
