import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '@environment/*';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class StatusService {
  constructor(private http: HttpClient) {}

  getStatus(): Observable<any> {
    const url: string = environment.api_url + '/status';
    const headers = new HttpHeaders().set('content-type', 'application/json');
    return this.http.get(url, { headers: headers });
  }
}
