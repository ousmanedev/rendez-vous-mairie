import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { PortalAppointmentService } from './portal-appointment.service';

describe('PortalAppointmentService', () => {
  let service: PortalAppointmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HttpClient],
    });
    service = TestBed.inject(PortalAppointmentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
