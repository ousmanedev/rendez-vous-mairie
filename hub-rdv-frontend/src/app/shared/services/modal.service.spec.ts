import { TestBed } from '@angular/core/testing';

import { ModalService } from './modal.service';

describe('ModalService', () => {
  let service: ModalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ModalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should emit showLoading event when showModal is called', () => {
    let emitSpy = spyOn(service.showLoading, 'emit');
    service.showModal('testName');
    expect(emitSpy).toHaveBeenCalledWith({
      loading: true,
      cpName: 'testName',
    });
  });

  it('should emit showGeoUndefined event when showModalGeoUndefined is called', () => {
    let emitSpy = spyOn(service.showGeoUndefined, 'emit');
    service.showModalGeoUndefined();
    expect(emitSpy).toHaveBeenCalledWith({
      showGeoUndefined: true,
    });
  });
});
