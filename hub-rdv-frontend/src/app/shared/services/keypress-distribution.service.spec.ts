import { TestBed } from '@angular/core/testing';

import { KeypressDistributionService } from './keypress-distribution.service';

describe('KeypressDistributionService', () => {
  let service: KeypressDistributionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KeypressDistributionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
