import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AddressGouvService } from './address-gouv.service';
import { HttpClient } from '@angular/common/http';

describe('AddressGouvService', () => {
  let service: AddressGouvService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HttpClient],
    });
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(AddressGouvService);
  });
  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call http get if getAddressByLonAndLat is called', () => {
    const httpGetSpy = spyOn(httpClient, 'get');
    service.getAddressByLonAndLat(2, 48);
    expect(httpGetSpy).toHaveBeenCalled();
  });

  it('should call http get if getLonAndLatByAddress is called', () => {
    const httpGetSpy = spyOn(httpClient, 'get');
    service.getLonAndLatByAddress('Paris');
    expect(httpGetSpy).toHaveBeenCalled();
  });
});
