import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PredemandeService {
  id_url: string = environment.api_url + '/searchApplicationIds?';
  constructor(private http: HttpClient) {}
  get(reCAPTCHA_token: string, application_ids?: Array<string>): Observable<any> {
    const headers = new HttpHeaders().set('content-type', 'application/json').set('reCAPTCHA_token', reCAPTCHA_token);
    // .set('Access-Control-Allow-Origin', '*')
    // .set('x-hub-rdv-auth-token', 'test-token');
    let url = this.id_url;
    application_ids?.forEach((Element: any) => (url = url + 'application_ids=' + Element + '&'));
    url = url.slice(0, -1);
    return this.http.get(url, { headers: headers });
  }
}
