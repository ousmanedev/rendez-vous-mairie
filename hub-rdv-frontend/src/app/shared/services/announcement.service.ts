import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AnnouncementService {
  id_url: string = environment.api_url + '/Announcement';

  constructor(private http: HttpClient) {}

  get(): Observable<any> {
    const headers = new HttpHeaders().set('content-type', 'application/json');
    return this.http.get(this.id_url, { headers: headers });
  }
}
