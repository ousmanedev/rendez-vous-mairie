import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DepartmentCitiesComponent } from './department-cities.component';
import { PortalAppointmentService } from '@shared/services/portal-appointment.service';
import { DeviceStateService } from '@shared/services/device-state.service';
import { Department } from '@shared/classes/Department';
import { of } from 'rxjs';

describe('DepartmentCitiesComponent', () => {
  let component: DepartmentCitiesComponent;
  let fixture: ComponentFixture<DepartmentCitiesComponent>;
  let portalAppointmentServiceSpy: PortalAppointmentService;
  let deviceStateSpy: DeviceStateService;
  let mockMunicipalities = [
    {
      id: '724',
      name: 'Mairie de Massy',
      longitude: 2.2713056,
      latitude: 48.7308016,
      public_entry_address: '1 avenue du G\u00e9n\u00e9ral De Gaulle',
      zip_code: '91300',
      city_name: 'Massy',
      website: 'https://esii-orion.com/orion-reservation?account=CFNZUQ',
      city_logo:
        'https://esii-orion.com/orion-admin/rs/media/library/download/CFNZUQ/614c8eef0322200f1c318ea5/logoName/t%C3%A9l%C3%A9chargement.png?1670920600211',
      distance_km: 15.3,
      department_name: 'Ein',
    },
  ];

  let departements = [
    { name: 'Corse', decoded_name: 'corse', code: '20', longitude: 8.73611111111111, latitude: 41.91972222222222 },
    { name: 'Ain', decoded_name: 'ain', code: '01', longitude: 5.221944444444444, latitude: 46.2025 },
    { name: 'Aisne', decoded_name: 'aisne', code: '02', longitude: 3.6225, latitude: 49.562222222222225 },
    { name: 'Allier', decoded_name: 'allier', code: '03', longitude: 3.334444444444444, latitude: 46.568333333333335 },
    {
      name: 'Alpes-de-Haute-Provence',
      decoded_name: 'alpes-de-haute-provence',
      code: '04',
      longitude: 6.238055555555555,
      latitude: 44.09222222222222,
    },
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DepartmentCitiesComponent],
      imports: [HttpClientTestingModule],
      providers: [HttpClient],
    }).compileComponents();

    fixture = TestBed.createComponent(DepartmentCitiesComponent);
    component = fixture.componentInstance;
    portalAppointmentServiceSpy = TestBed.inject(PortalAppointmentService);
    deviceStateSpy = TestBed.inject(DeviceStateService);

    fixture.detectChanges();
  });
  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set the content description and set a title and load a spinner when ngOnInit is called', () => {
    spyOn(Department, 'getAllDepartments').and.returnValue(departements);
    spyOn(portalAppointmentServiceSpy, 'getMunicipalities').and.returnValue(of(mockMunicipalities));
    const spy = spyOnProperty(deviceStateSpy, 'onResize$', 'get').and.callFake(() => of(true));
    spyOn(Department, 'findDepartmentByDecodedName').and.returnValue(departements[0]);
    // act
    component.ngOnInit();
    fixture.detectChanges();
    // assert
    expect(portalAppointmentServiceSpy.getMunicipalities).toHaveBeenCalledTimes(1);
    expect(Department.getAllDepartments).toHaveBeenCalled();
    expect(Department.findDepartmentByDecodedName).toHaveBeenCalled();
    expect(spy).toHaveBeenCalled();
  });
});
