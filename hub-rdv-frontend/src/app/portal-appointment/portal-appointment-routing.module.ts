import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PortalAppointmentComponent } from './portal-appointment/portal-appointment.component';
import { CitiesComponent } from './cities/cities.component';
import { DepartmentsComponent } from './departments/departments.component';

const routes: Routes = [
  { path: '', component: PortalAppointmentComponent },
  { path: 'ville/:cityName', component: PortalAppointmentComponent },
  { path: 'departement/:departmentName', component: PortalAppointmentComponent },
  { path: 'villes', component: CitiesComponent },
  { path: 'departements', component: DepartmentsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PortalAppointmentRoutingModule {}
