import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { SearchByCityComponent } from './search-by-city.component';
import { Meta, Title } from '@angular/platform-browser';
import { DeviceStateService } from '@shared/services/device-state.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AddressGouvService } from '@shared/services/address-gouv.service';
import { SEOService } from '@shared/services/seoservice.service';
import { PortalAppointmentService } from '@shared/services/portal-appointment.service';
import { of } from 'rxjs';
describe('SearchByCityComponent', () => {
  let component: SearchByCityComponent;
  let fixture: ComponentFixture<SearchByCityComponent>;
  let titleServiceSpy: Title;
  let deviceStateSpy: DeviceStateService;
  let spinnerSpy: NgxSpinnerService;
  let addressServiceSpy: AddressGouvService;
  let seoserviceSpy: SEOService;
  let portalAppointmentServiceSpy: PortalAppointmentService;
  let metaTagServiceSpy: Meta;
  let mockMunicipality = {
    id: '724',
    name: 'Mairie de Massy',
    longitude: 2.2713056,
    latitude: 48.7308016,
    public_entry_address: '1 avenue du G\u00e9n\u00e9ral De Gaulle',
    zip_code: '91300',
    city_name: 'Massy',
    website: 'https://esii-orion.com/orion-reservation?account=CFNZUQ',
    city_logo:
      'https://esii-orion.com/orion-admin/rs/media/library/download/CFNZUQ/614c8eef0322200f1c318ea5/logoName/t%C3%A9l%C3%A9chargement.png?1670920600211',
    distance_km: 15.3,
    department_name: 'Ein',
  };
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SearchByCityComponent],
      imports: [ReactiveFormsModule, HttpClientModule, RouterTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(SearchByCityComponent);
    component = fixture.componentInstance;
    titleServiceSpy = TestBed.inject(Title);
    portalAppointmentServiceSpy = TestBed.inject(PortalAppointmentService);
    deviceStateSpy = TestBed.inject(DeviceStateService);
    metaTagServiceSpy = TestBed.inject(Meta);
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set dynamique Title by city name when ngOnInit ', () => {
    spyOn(titleServiceSpy, 'setTitle');
    spyOn(metaTagServiceSpy, 'updateTag');
    spyOn(portalAppointmentServiceSpy, 'getMunicipality').and.returnValue(of(mockMunicipality));
    const spy = spyOnProperty(deviceStateSpy, 'onResize$', 'get').and.callFake(() => of(true));
    component.ngOnInit();
    fixture.detectChanges();
    expect(titleServiceSpy.setTitle).toHaveBeenCalled();
    expect(metaTagServiceSpy.updateTag).toHaveBeenCalled();
    expect(portalAppointmentServiceSpy.getMunicipality).toHaveBeenCalled();
    expect(spy).toHaveBeenCalled();
  });
});
