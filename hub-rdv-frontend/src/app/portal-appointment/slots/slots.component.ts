import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  SimpleChanges,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { Slot } from '@shared/classes/slot';
import { SearchCriteria } from '@shared/classes/search-criteria';
import { Subscription } from 'rxjs';
import { DeviceStateService } from '@shared/services/device-state.service';
import { KeypressDistributionService } from '@shared/services/keypress-distribution.service';
import { KeyAction } from '@shared/enums/key-action';
import { Utils } from '@shared/utils/utils';

export enum KEY_CODE {
  RIGHT_ARROW = 39,
  LEFT_ARROW = 37,
}

@Component({
  selector: 'rdv-slots',
  templateUrl: './slots.component.html',
  styleUrls: ['./slots.component.scss'],
})
export class SlotsComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {
  @Input() slots: Map<string, Array<Slot>> | undefined;
  //@Input() OfflineMunicipalities?: Map<string, Array<Slot>>;
  //@Input() displayOfflineFirst?: Boolean;
  @Input() onlyOfflineMunicipalities?: Boolean;
  @Input() rawOfflineMunicipalities?: Array<any>;
  @Input() endOfSearch?: boolean;
  @Input() searchCriteria?: SearchCriteria;
  @Input() results_length?: number;
  @Input() sortBy: string = 'Date';
  @Input() message: string = '';
  @Input() non_response_percentage_from_editors?: number;
  @Input() searchByDepartment?: boolean;
  @Input() isLoadingSearchResult?: boolean;
  @Output() sortByEvent: EventEmitter<string>;

  subscription?: Subscription;
  start: number = 0;
  end: number = 7;
  step: number = 7;
  previousTab: number = 0;
  currentTab: number = 0;
  currentTabDate: string | undefined;
  deltaMsSelect = 0;

  address: string = '';
  @ViewChildren('buttons') buttons?: QueryList<ElementRef>;
  @ViewChild('tabpanels') tabpanel?: QueryList<ElementRef>;

  /* Mobile*/
  OfflineMunicipalities_: Map<string, Array<Slot>> = new Map();
  limiter: number = 10;
  @Input() localisedByTpe: Boolean = true;

  /* Used to control accessibility navigation */
  private accessibilityTimeoutId: ReturnType<typeof setTimeout> | undefined;
  private previouslyFocusedElement: Element | undefined;

  __getLocaleTimeFormat__ = Utils.__getLocaleTimeFormat__;

  constructor(
    private deviceState: DeviceStateService,
    private keyService: KeypressDistributionService
  ) {
    this.onScreenChange();
    this.sortByEvent = new EventEmitter<string>();
  }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    //const isTabActiveElement = document.activeElement?.classList?.contains('fr-tab');
    switch (event.key) {
      case KeyAction.ArrowRight:
        //if (!isTabActiveElement) return;
        if (this.currentTab + 1 < this.end - this.start && this.start != 0) this.select(this.currentTab + 1);
        else if (this.slots && this.start == 0 && this.rawOfflineMunicipalities?.length && this.currentTab + 1 < this.end - this.start)
          this.select(this.currentTab + 1);
        else if (this.end != this.slots?.size) {
          this.next();
        }
        //else this.onScreenChange()
        break;

      case KeyAction.ArrowLeft:
        //if (!isTabActiveElement) return;
        if (this.currentTab > 0 || (this.start == 0 && this.rawOfflineMunicipalities?.length && this.currentTab == 0)) {
          this.select(this.currentTab - 1);
        } else if (this.currentTab == 0) this.previous();
        break;
      case 'Tab':
        this.accessibilityNavigationAfterTabulation();
        break;
    }
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    this.subscription = this.deviceState.resize().subscribe((evt) => {
      this.onScreenChange();
    });

    if (this.results_length && this.searchCriteria && changes['results_length']?.currentValue != changes['results_length']?.previousValue) {
      let index = 0;

      if (this.currentTabDate && this.currentTab !== this.previousTab) {
        let found: Boolean = false;
        while (!found) {
          const iterator = this.keys(this.slots)?.values();
          index = 0;
          if (iterator) {
            for (const value of iterator) {
              if (value == this.currentTabDate) {
                found = true;
                break;
              } else index = index + 1;
            }
            if (!found) {
              this.next();
            } else {
              setTimeout(() => {
                this.select(index);
              }, 200);
              break;
            }
          }
        }
      } else {
        this.select(0);
      }
    }
  }

  ngAfterViewInit(): void {
    this.keyService.keyEventObs.subscribe((event) => {
      switch (event.key) {
        case KeyAction.ArrowRight:
          if (this.currentTab + 1 < this.end - this.start && this.start != 0) this.select(this.currentTab + 1);
          else if (this.slots && this.start == 0 && this.rawOfflineMunicipalities?.length && this.currentTab + 1 < this.end - this.start)
            this.select(this.currentTab + 1);
          else if (this.end != this.slots?.size) {
            this.next();
          }
          //else this.onScreenChange()
          break;

        case KeyAction.ArrowLeft:
          if (this.currentTab > 0 || (this.start == 0 && this.rawOfflineMunicipalities?.length && this.currentTab == 0)) {
            this.select(this.currentTab - 1);
          } else if (this.currentTab == 0) this.previous();
          break;
        case 'Tab':
          break;
      }
    });
  }

  keys(map: Map<any, any> | undefined) {
    if (map !== undefined) {
      let keys = [...map.keys()];
      if (this.sortBy == 'Date') return this.sortDates(keys).slice(this.start, this.end);
      else return this.sortDistance(keys).slice(this.start, this.end);
    }
    return undefined;
  }

  sortDates(list: Array<string>) {
    return list.sort((a, b) => {
      return <any>new Date(a) - <any>new Date(b);
    });
  }

  sortDistance(list: Array<string>) {
    return list.sort((a, b) => {
      return <any>Number(a) - <any>Number(b);
    });
  }

  accessibilityNavigationAfterTabSelect(index: number) {
    // debouncing
    if (this.accessibilityTimeoutId) {
      clearTimeout(this.accessibilityTimeoutId);
    }
    this.accessibilityTimeoutId = setTimeout(() => {
      const currentTab = document.getElementById('tabpanel' + index);
      if (!currentTab) return;

      this.accessibilityTimeoutId = undefined;

      currentTab.setAttribute('tabindex', '0');
      currentTab.focus();

      this.previouslyFocusedElement = currentTab;
      document.querySelectorAll('.fr-tab').forEach((tab) => {
        if (tab.getAttribute('id') !== currentTab?.getAttribute('id')) {
          tab.setAttribute('tabindex', '-1');
        }
      });
      this.accessibilityNavigationAfterTabulation();
    }, 100);
  }

  accessibilityNavigationAfterTabulation() {
    const isTabNewFocusedElement = document.activeElement?.classList?.contains('fr-tab');
    const tabWasPreviouslyFocused = this.previouslyFocusedElement ? this.previouslyFocusedElement.classList.contains('fr-tab') : false;
    let tabindex: string | undefined;
    if (isTabNewFocusedElement) {
      tabindex = '-1';
    } else if (tabWasPreviouslyFocused) {
      tabindex = '0';
    }
    if (tabindex) {
      const value = tabindex;
      document.querySelectorAll('.rdv-disableable-tabindex').forEach((e) => {
        e.setAttribute('tabindex', value);
      });
    }
  }

  @HostListener('window:focusout', ['$event'])
  blurEvent(event: FocusEvent) {
    this.previouslyFocusedElement = undefined;
    if (event.target instanceof Element) {
      this.previouslyFocusedElement = event.target as Element;
    }
  }

  select(index: number) {
    this.accessibilityNavigationAfterTabSelect(index);

    if (this.previousTab != this.currentTab) this.previousTab = this.currentTab;
    this.currentTab = index;
    this.currentTabDate = this.keys(this.slots)?.[index];
    if (this.previousTab !== index) {
      let previous_tab: HTMLElement | null = document.getElementById('tabpanel' + this.previousTab);
      let current_tab = document.getElementById('tabpanel' + index);

      if (previous_tab) previous_tab.setAttribute('aria-selected', 'false');
      if (current_tab) {
        current_tab.setAttribute('aria-selected', 'true');
      }

      let previous = document.getElementById('tab' + this.previousTab);
      let elm = document.getElementById('tab' + index);

      if (previous && elm) {
        //previous.className = 'toggle-over';
        elm.classList.add('toggle-left');
        elm.style.display = 'inline-block';
        previous.style.display = 'none';
      } else if (elm) {
        elm.style.transform = 'translate(0%,200%)';
        elm.classList.add('toggle-over');
      }
    }
  }

  next() {
    if (this.slots) {
      if (this.end < this.slots.size) {
        this.currentTab = 0;
        this.previousTab = 0;
        if (this.end + this.step > this.slots.size) this.end = this.slots.size;
        else this.end += this.step;
        this.start += this.step;
        if (!this.currentTabDate) this.currentTabDate = this.keys(this.slots)?.[0];
      }
    }
  }

  previous() {
    if (this.start - this.step >= 0) {
      if (this.slots && this.end + this.step >= this.slots.size) this.onScreenChange();

      this.currentTab = 0;
      this.previousTab = 0;
      this.start -= this.step;
      this.end -= this.step;
      this.currentTabDate = this.keys(this.slots)?.[0];
    }
  }

  getLastElementIndex(): number {
    if (this.slots) {
      return this.slots.size - 1;
    }
    return 0;
  }

  onScreenChange() {
    const screen = this.deviceState.getScreen();
    switch (screen) {
      case 'MD':
        this.step = 2;
        this.end = this.start + 2;
        break;
      case 'LG':
        this.step = 3;
        this.end = this.start + 3;
        break;
      case 'XL':
        this.step = 5;
        this.end = this.start + 5;
        break;
      default: /* mode mobile <768 */
        this.step = 2;
        this.end = Number.MAX_VALUE;
        break;
    }
  }

  identify(index: any, item: any) {
    return item;
  }

  toDate(strDate: string) {
    return new Date(strDate);
  }
  onChange(event: any) {
    this.sortBy = event.target.value;
    this.start = 0;
    this.currentTab = 0;
    this.onScreenChange();
    this.sortByEvent.emit(event.target.value);
  }

  StringToNumber(input: string) {
    const numeric = Number(input);
    return numeric;
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

  getTenSlots(slotsPerTabs: any): any {
    if (this.slots) return slotsPerTabs?.slice(0, this.limiter);
    return;
  }
  setModalUrl(url: any) {
    let Modal = document.getElementById('fr-modal-2-title') as HTMLElement;
    if (Modal) {
      if (Modal.innerText != 'Vous allez être redirigé vers le site de prise de rendez-vous.')
        Modal.innerText = 'Vous allez être redirigé vers le site de prise de rendez-vous.';
    }

    let link = document.getElementById('slotLink') as HTMLAnchorElement;
    if (link) link.href = url;
  }
  setModalUrl2(mairie: string, url: any) {
    let Modal = document.getElementById('fr-modal-2-title') as HTMLElement;
    if (Modal)
      Modal.innerText =
        'Vous allez être redirigé vers le site de ' +
        mairie +
        ' afin de consulter les éventuelles disponibilités de rendez-vous CNI/Passeport.';
    let link = document.getElementById('slotLink') as HTMLAnchorElement;
    if (link) link.href = url;
  }

  formatAddress(address: string | undefined) {
    if (address) {
      const array = address.split(' ');
      array.splice(-1);
      return array.join(' ');
    }
    return undefined;
  }

  onLoadMore() {
    if (this.slots) this.limiter = this.limiter + 10;
  }

  getLength(slotsPerTabs: Array<any> | undefined): any {
    if (slotsPerTabs) return slotsPerTabs.length;
    return;
  }

  ani($event: any, index: any) {
    let belm = document.getElementById('button-' + index);
    let elm = document.getElementById('accordion-' + index);

    if (elm && belm) {
      belm.setAttribute('aria-expanded', belm.getAttribute('aria-expanded') === 'false' ? 'true' : 'false');
      elm.classList.toggle('toggle');
    }
  }
}
