import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { AddressGouvService } from '@shared/services/address-gouv.service';
import { GeolocalisationService } from '@shared/services/geolocalisation.service';
import { DateUtils } from '@shared/utils/date';

import { SearchComponent } from './search.component';
import { SEOService } from '@shared/services/seoservice.service';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let injectedGeolocalisationService: GeolocalisationService;
  let injectedAdressGouvService: AddressGouvService;
  let injectedHttpClint: HttpClient;
  let seoserviceSpy: SEOService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SearchComponent],
      imports: [ReactiveFormsModule, HttpClientModule],
      providers: [],
    }).compileComponents();

    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    seoserviceSpy = TestBed.inject(SEOService);
    fixture.detectChanges();

    injectedGeolocalisationService = TestBed.get(GeolocalisationService);
    injectedAdressGouvService = TestBed.get(AddressGouvService);
    injectedHttpClint = TestBed.get(HttpClient);
  });
  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should updateCanonicalUrl when ngOnInt is executed ', () => {
    let updateCanonicalUrlSpy = spyOn(seoserviceSpy, 'updateCanonicalUrl');
    component.ngOnInit();
    expect(updateCanonicalUrlSpy).toHaveBeenCalled();
  });

  it('should emit searchCriteria if form is valid', () => {
    // Arrange
    component.searchRdvForm.setValue({
      start_date: DateUtils.getDefaultStartDate(),
      end_date: DateUtils.getDefaultEndDate(undefined),
      radius_km: 20,
      longitude: 2,
      latitude: 40,
      address: 'Paris',
      reason: 'CNI',
      documents_number: 1,
    });
    let emitSpy = spyOn(component.searchCriteria, 'emit');
    // Act
    component.onSearch();
    // Assert
    expect(emitSpy).toHaveBeenCalledWith({ ...component.searchRdvForm.value });
  });

  it('should subscribe to geolocalisation if getUserLocation is called', () => {
    let getCurrentPositionSpy = spyOn(injectedGeolocalisationService, 'getCurrentPosition').and.returnValue(
      new GeolocalisationService().getCurrentPosition()
    );
    component.getUserLocation();
    //expect(getCurrentPositionSpy).toHaveBeenCalled();
  });

  it('should call getLonAndLatByAddress when onChangeSearch called and search length > 0', () => {
    let getLonAndLatByAddressSpy = spyOn(injectedAdressGouvService, 'getLonAndLatByAddress').and.returnValue(
      new AddressGouvService(injectedHttpClint).getLonAndLatByAddress()
    );
    component.onChangeSearch('par');
    expect(getLonAndLatByAddressSpy).toHaveBeenCalledOnceWith('par', 'municipality');
  });

  it('should not call getLonAndLatByAddress when onChangeSearch called and search length = 0', () => {
    let getLonAndLatByAddressSpy = spyOn(injectedAdressGouvService, 'getLonAndLatByAddress').and.returnValue(
      new AddressGouvService(injectedHttpClint).getLonAndLatByAddress()
    );
    component.onChangeSearch('');
    expect(getLonAndLatByAddressSpy).not.toHaveBeenCalled();
  });

  it('should not call getLonAndLatByAddress if selectEvent called on item that already has coordinates', () => {
    let getLonAndLatByAddressSpy = spyOn(injectedAdressGouvService, 'getLonAndLatByAddress').and.returnValue(
      new AddressGouvService(injectedHttpClint).getLonAndLatByAddress()
    );
    component.selectEvent({
      tagName: 'Paris 10',
      coordinates: [2, 42],
    });
    expect(getLonAndLatByAddressSpy).not.toHaveBeenCalled();
    expect(component.searchRdvForm.get('address')?.value).toBe('Paris 10');
  });

  it("should call getLonAndLatByAddress if selectEvent called on item doesn't have coordinates", () => {
    let getLonAndLatByAddressSpy = spyOn(injectedAdressGouvService, 'getLonAndLatByAddress').and.returnValue(
      new AddressGouvService(injectedHttpClint).getLonAndLatByAddress()
    );
    component.selectEvent({
      tagName: 'Paris 10',
    });
    expect(getLonAndLatByAddressSpy).toHaveBeenCalled();
    expect(component.searchRdvForm.get('address')?.value).toBe('Paris 10');
  });

  it('should empty form latitude and longitude and focus autocomplete when onCleared is called', () => {
    expect(component.searchRdvForm.get('longitude')?.value).toBe(null);
    expect(component.searchRdvForm.get('latitude')?.value).toBe(null);
    component.searchRdvForm.get('longitude')?.setValue(2);
    component.searchRdvForm.get('latitude')?.setValue(48);
    component.auto = {
      focus: () => {},
    };
    let focusSpy = spyOn(component.auto, 'focus');
    component.onCleared({});
    expect(component.searchRdvForm.get('longitude')?.value).toBe(null);
    expect(component.searchRdvForm.get('latitude')?.value).toBe(null);
    expect(component.address).toBe('');
    expect(focusSpy).toHaveBeenCalled();
  });

  it('should return array of tagNames when customFilter is called', () => {
    let data = [
      {
        tagName: 'Paris',
      },
      {
        tagName: 'Marseille',
      },
    ];
    const result = component.customFilter(data, 'some-text');
    expect(result).toEqual(data);
  });

  it('should update all form fields when setFormData is called', () => {
    component.setFormData('2022-01-01', '2022-03-01', 40, 2, 48, 'Paris', 1, 'CNI');
    expect(component.searchRdvForm.get('start_date')?.value).toBe('2022-01-01');
    expect(component.searchRdvForm.get('end_date')?.value).toBe('2022-03-01');
    expect(component.searchRdvForm.get('radius_km')?.value).toBe(40);
    expect(component.searchRdvForm.get('longitude')?.value).toBe(48);
    expect(component.searchRdvForm.get('latitude')?.value).toBe(2);
    expect(component.address).toBe('Paris');
    expect(component.searchRdvForm.get('documents_number')?.value).toBe(1);
    expect(component.searchRdvForm.get('reason')?.value).toBe('CNI');
  });

  it('should update enda date if start date is changed', () => {
    let end_date = component.searchRdvForm.get('end_date')?.value;
    let dateMax = component.dateMax;
    component.searchRdvForm.get('start_date')?.setValue('2150-01-01');
    expect(component.searchRdvForm.get('end_date')?.value).not.toEqual(end_date);
    expect(component.dateMax).not.toEqual(dateMax);
  });

  it('should update all form fields when setFormData is called', () => {
    component.setFormData('2022-01-01', '2022-03-01', 40, 2, 48, 'Paris', 1, 'CNI');
    expect(component.searchRdvForm.get('start_date')?.value).toBe('2022-01-01');
    expect(component.searchRdvForm.get('end_date')?.value).toBe('2022-03-01');
    expect(component.searchRdvForm.get('radius_km')?.value).toBe(40);
    expect(component.searchRdvForm.get('longitude')?.value).toBe(48);
    expect(component.searchRdvForm.get('latitude')?.value).toBe(2);
    expect(component.address).toBe('Paris');
    expect(component.searchRdvForm.get('documents_number')?.value).toBe(1);
    expect(component.searchRdvForm.get('reason')?.value).toBe('CNI');
  });
});
