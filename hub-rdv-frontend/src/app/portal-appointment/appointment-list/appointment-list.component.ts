import { Component, Input, OnInit } from '@angular/core';
import { Slot } from '@shared/classes/slot';
import { Utils } from '@shared/utils/utils';

@Component({
  selector: 'rdv-appointment-list',
  templateUrl: './appointment-list.component.html',
  styleUrls: ['./appointment-list.component.scss'],
})
export class AppointmentListComponent implements OnInit {
  @Input() slotsPerTabs?: Array<Slot>;
  @Input() tab?: any;
  @Input() sortBy?: any;

  timeZone: any;
  local: any;
  __getLocaleTimeFormat__ = Utils.__getLocaleTimeFormat__;

  constructor() {
    this.timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
    this.local = Intl.DateTimeFormat().resolvedOptions().locale;
    this.tab?.toLocaleDateString(this.local);
  }

  ngOnInit(): void {}
  // traitement supplimentaire doit faire directement dasnle

  setModalUrl(url: any) {
    let Modal = document.getElementById('fr-modal-2-title') as HTMLElement;
    if (Modal) {
      if (Modal.innerText != 'Vous allez être redirigé vers le site de prise de rendez-vous.')
        Modal.innerText = 'Vous allez être redirigé vers le site de prise de rendez-vous.';
    }
    let link = document.getElementById('slotLink') as HTMLAnchorElement;
    if (link) link.href = url;
  }

  setModalUrl2(mairie: string | undefined, url: any) {
    let Modal = document.getElementById('fr-modal-2-title') as HTMLElement;
    if (Modal)
      Modal.innerText =
        'Vous allez être redirigé vers le site de ' +
        mairie +
        ' afin de consulter les éventuelles disponibilités de rendez-vous CNI/Passeport.';
    let link = document.getElementById('slotLink') as HTMLAnchorElement;
    if (link) link.href = url;
  }

  toDate(strDate: string) {
    return new Date(strDate);
  }
  limiter: number = 10;
  getTenSlots(slotsPerTabs: Array<Slot>): Array<Slot> | undefined {
    if (this.slotsPerTabs) return this.slotsPerTabs?.slice(0, this.limiter);
    return;
  }
  onLoadMore() {
    if (this.slotsPerTabs) this.limiter = this.limiter + 10;
  }
  onLoadLess() {
    if (this.slotsPerTabs && this.limiter > 10) this.limiter = this.limiter - 10;
  }
  getTitle(slotDate: any, slotHour: any, addressMunicipality: any, offline: boolean) {
    if (offline) {
      return `Accéder au site internet de ${addressMunicipality} pour consulter leurs disponibilités.`;
    } else {
      if (this.sortBy == 'Distance') slotDate = new Date(slotDate);
      return `Réserver le ${slotDate?.toLocaleDateString('fr')} à ${slotHour} dans ${addressMunicipality}`;
    }
  }
  getOfflineMeetingPointTitle(Municipality: any) {
    return 'Site internet de ' + Municipality;
  }
}
