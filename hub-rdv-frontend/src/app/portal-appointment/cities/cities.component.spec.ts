import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, flush, fakeAsync } from '@angular/core/testing';

import { CitiesComponent } from './cities.component';
import { PortalAppointmentService } from '@shared/services/portal-appointment.service';
import { Meta, Title } from '@angular/platform-browser';
import { of } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

describe('CitiesComponent', () => {
  let component: CitiesComponent;
  let fixture: ComponentFixture<CitiesComponent>;
  let portalAppointmentServiceSpy: PortalAppointmentService;
  let metaServiceSpy: Meta;
  let spinnerSpy: NgxSpinnerService;
  let titleSpy: Title;
  let mockMunicipalities = [
    {
      id: '724',
      name: 'Mairie de Massy',
      longitude: 2.2713056,
      latitude: 48.7308016,
      public_entry_address: '1 avenue du G\u00e9n\u00e9ral De Gaulle',
      zip_code: '91300',
      city_name: 'Massy',
      website: 'https://esii-orion.com/orion-reservation?account=CFNZUQ',
      city_logo:
        'https://esii-orion.com/orion-admin/rs/media/library/download/CFNZUQ/614c8eef0322200f1c318ea5/logoName/t%C3%A9l%C3%A9chargement.png?1670920600211',
      distance_km: 15.3,
      department_name: 'Ein',
    },
  ];
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CitiesComponent],
      imports: [HttpClientTestingModule],
      providers: [HttpClient],
    }).compileComponents();

    fixture = TestBed.createComponent(CitiesComponent);
    component = fixture.componentInstance;
    portalAppointmentServiceSpy = TestBed.inject(PortalAppointmentService);
    metaServiceSpy = TestBed.inject(Meta);
    spinnerSpy = TestBed.inject(NgxSpinnerService);
    titleSpy = TestBed.inject(Title);
    // spyOn(portalAppointmentServiceSpy, 'getMunicipalities').and.returnValue(of(mockMunicipalities))
    // spyOn(spinnerSpy , 'show')
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set the content description and set a title and load a spinner when ngOnInit is called', () => {
    spyOn(spinnerSpy, 'show').and.returnValue(Promise.resolve(true));
    spyOn(portalAppointmentServiceSpy, 'getMunicipalities').and.returnValue(of(mockMunicipalities));
    spyOn(metaServiceSpy, 'updateTag');
    spyOn(titleSpy, 'setTitle');
    // act
    component.ngOnInit();
    fixture.detectChanges();
    // assert
    expect(portalAppointmentServiceSpy.getMunicipalities).toHaveBeenCalledTimes(1);
    expect(spinnerSpy.show).toHaveBeenCalled();
    expect(metaServiceSpy.updateTag).toHaveBeenCalled();
    expect(titleSpy.setTitle).toHaveBeenCalled();
  });

  // it('should call onClick method when we click on city name', () => {
  //   spyOn(portalAppointmentServiceSpy, 'getMunicipalities').and.returnValue(of(mockMunicipalities));
  //   spyOn(spinnerSpy, 'show').and.returnValue(Promise.resolve(true));
  //   spyOn(metaServiceSpy, 'updateTag');
  //   spyOn(titleSpy, 'setTitle');

  //   let ahref = fixture.debugElement.nativeElement.querySelector('a');
  //   ahref.click();
  //   fixture.whenStable().then(() => {
  //     expect(component.onClick).toHaveBeenCalled();
  //   });
  // });
});
