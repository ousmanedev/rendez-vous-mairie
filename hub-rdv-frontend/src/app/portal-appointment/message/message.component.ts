import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'rdv-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent implements OnInit {
  @Input() searchByDepartment: boolean | undefined = false;
  @Input() endOfSearch: boolean | undefined = false;
  @Input() isLoadingSearchResult: boolean | undefined = false;
  @Input() nbResult: number | undefined = 0;
  @Input() message: string = '';
  @Input() non_response_percentage_from_editors?: number | undefined;
  @Input() type: string = 'Desktop';
  constructor() {}

  ngOnInit(): void {}
}
