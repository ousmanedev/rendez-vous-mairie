import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { ReCaptchaV3Service } from 'ng-recaptcha';
import { PortalAppointmentComponent } from './portal-appointment.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { WebsocketService } from '@shared/services/websocket.service';
import { fakeWebsocketService } from '@shared/mocks/mock.websocket.service';
import { fakeDeviceStateService } from '@shared/mocks/mock.device-state.service';
import { By } from '@angular/platform-browser';
import { fakeRecaptchaV3Service } from '@shared/mocks/mock.recaptcha.service';
import { RouterTestingModule } from '@angular/router/testing';
import { DeviceStateService } from '@shared/services/device-state.service';
import { Subscription } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('PortalAppointmentComponent', () => {
  let component: PortalAppointmentComponent;
  let fixture: ComponentFixture<PortalAppointmentComponent>;
  let websocketInjectedService: WebsocketService;
  let deviceStateInjectedService: DeviceStateService;
  let recaptchaV3InjectedService: ReCaptchaV3Service;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PortalAppointmentComponent],
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [
        {
          provide: ReCaptchaV3Service,
          useValue: fakeRecaptchaV3Service,
        },
        {
          provide: WebsocketService,
          useValue: fakeWebsocketService,
        },
        {
          provide: DeviceStateService,
          useValue: fakeDeviceStateService,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(PortalAppointmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    jasmine.clock().install();

    websocketInjectedService = TestBed.get(WebsocketService);
    deviceStateInjectedService = TestBed.get(DeviceStateService);
    recaptchaV3InjectedService = TestBed.get(ReCaptchaV3Service);

    spyOn(deviceStateInjectedService, 'isMobileResolution').and.returnValue(false);
  });

  afterEach(function () {
    fixture.destroy();
    jasmine.clock().uninstall();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('renders Desktop Search Component', () => {
    const desktopSearchElement = fixture.debugElement.query(By.css('rdv-search'));
    expect(desktopSearchElement).toBeTruthy();
  });

  it('passes the searchCriteria to Desktop Search Component', () => {
    const desktopSearchElement = fixture.debugElement.query(By.css('rdv-search'));
    expect(desktopSearchElement.properties['searchCriteria']).toBe(component.searchCriteria);
  });

  it('handles the output search event from Desktop Search Component', () => {
    const desktopSearchElement = fixture.debugElement.query(By.css('rdv-search'));
    spyOn(component, 'onSearch');
    const searchCriteria = {
      longitude: 20,
      latitude: 20,
      reason: 'CNI',
    };
    desktopSearchElement.triggerEventHandler('searchCriteria', searchCriteria);
    expect(component.onSearch).toHaveBeenCalledWith(searchCriteria);
  });

  it('sends search criteria through websocket if onSearch called', () => {
    const searchCriteria = {
      latitude: 2,
      longitude: 50,
      radius_km: 40,
      start_date: '2022-01-01',
      end_date: '2022-04-01',
      reason: 'CNI',
    };
    component.onSearch(searchCriteria);
    expect(component.isLoadingSearchResult).toBeTruthy();
  });

  it('should stop spinner if end_of_search received from websocket', () => {
    const searchCriteria = {
      latitude: 2,
      longitude: 50,
      radius_km: 40,
      start_date: '2022-01-01',
      end_date: '2022-04-01',
      reason: 'CNI',
    };
    let subscribeSpy = spyOn<any>(websocketInjectedService.messageSubject, 'subscribe').and.callFake((arg: any) => {
      arg['next']({
        data: JSON.stringify([
          {
            id: '724',
            name: 'Mairie de Massy',
            longitude: 2.2713056,
            latitude: 48.7308016,
            public_entry_address: '1 avenue du G\u00e9n\u00e9ral De Gaulle',
            zip_code: '91300',
            city_name: 'Massy',
            website: 'https://esii-orion.com/orion-reservation?account=CFNZUQ',
            city_logo:
              'https://esii-orion.com/orion-admin/rs/media/library/download/CFNZUQ/614c8eef0322200f1c318ea5/logoName/t%C3%A9l%C3%A9chargement.png?1670920600211',
            distance_km: 15.3,
            available_slots: [
              { datetime: '2023-02-17T09:30:00Z', callback_url: 'https://esii-orion.com/orion-reservation?account=CFNZUQ' },
              { datetime: '2023-02-20T08:30:00Z', callback_url: 'https://esii-orion.com/orion-reservation?account=CFNZUQ' },
              { datetime: '2023-02-20T08:50:00Z', callback_url: 'https://esii-orion.com/orion-reservation?account=CFNZUQ' },
              { datetime: '2023-02-20T09:10:00Z', callback_url: 'https://esii-orion.com/orion-reservation?account=CFNZUQ' },
              { datetime: '2023-02-20T09:30:00Z', callback_url: 'https://esii-orion.com/orion-reservation?account=CFNZUQ' },
            ],
          },
        ]),
      });
    });
    component.onSearch(searchCriteria);
    expect(component.endOfSearch).toBeFalse();

    /*
    subscribeSpy.and.callFake((arg: any) => {
      arg['next']({ data: JSON.stringify({ step: 'end_of_search', editors_number: 7, editor_errors_number: 0}) });
    });

    component.onSearch(searchCriteria);
    expect(component.endOfSearch).toBeTrue();
    */
  });

  it('should switch isLoadingSearchResult to false if subscription is defined', () => {
    component.isLoadingSearchResult = true;
    component.subscription = new Subscription();
    const searchCriteria = {
      latitude: 2,
      longitude: 50,
      radius_km: 40,
      start_date: '2022-01-01',
      end_date: '2022-04-01',
      reason: 'CNI',
    };
    component.onSearch(searchCriteria);
    expect(component.isLoadingSearchResult).toBeFalse();
  });

  it('should send searchCriteria if recaptcha callsback triggered', () => {
    let recaptchaServiceSpy = spyOn(recaptchaV3InjectedService, 'execute').and.returnValue(<any>{
      subscribe: (callbackObj: any) => {
        callbackObj['next']('123451TOKEN');
      },
    });
    let websocketNextSpy = spyOn(<any>websocketInjectedService.messageSubject, 'next');
    const searchCriteria = {
      latitude: 2,
      longitude: 50,
      radius_km: 40,
      start_date: '2022-01-01',
      end_date: '2022-04-01',
      reason: 'CNI',
    };
    component.onSearch(searchCriteria);
    expect(websocketNextSpy).toHaveBeenCalledOnceWith(searchCriteria);
  });

  it('should end spinning recaptcha callsback triggered and websocket throws error', () => {
    let recaptchaServiceSpy = spyOn(recaptchaV3InjectedService, 'execute').and.returnValue(<any>{
      subscribe: (callbackObj: any) => {
        callbackObj['next']('123451TOKEN');
      },
    });
    let websocketNextSpy = spyOn(<any>websocketInjectedService.messageSubject, 'next').and.callFake((searchCriteria: any) => {
      expect(component.endOfSearch).toBeFalse();
      throw Error('Test error');
    });
    const searchCriteria = {
      latitude: 2,
      longitude: 50,
      radius_km: 40,
      start_date: '2022-01-01',
      end_date: '2022-04-01',
      reason: 'CNI',
    };
    component.onSearch(searchCriteria);
    expect(component.endOfSearch).toBeTrue();
  });

  it('should detect websocketService change of connexion status', fakeAsync(() => {
    const okResponse = new Response(JSON.stringify({}), { status: 200, statusText: 'OK' });
    spyOn(window, 'fetch').and.resolveTo(okResponse);
    expect(component.errorMessage.title).toBeFalsy();
    websocketInjectedService.isConnected.emit({
      connected: false,
    });
    tick(100);
    expect(component.errorMessage.title).toBe('Connexion');
    websocketInjectedService.isConnected.emit({
      connected: true,
    });
  }));

  it('should set error message if checkOverload called and no results yet', () => {
    component.slots = new Map();
    component.endOfSearch = false;
    expect(component.errorMessage.title).toBeFalsy();
    component.checkOverload();
    expect(component.errorMessage.title).toBeTruthy();
  });

  /*
  it('should detect no internet connexion when websocket is off', fakeAsync(() => {
    let isOnlineSpy = spyOn<any, any>(fakeDeviceStateService, 'isOnline').and.returnValue(false);
    expect(component.errorMessage.title).toBeFalsy();
    websocketInjectedService.isConnected.emit({
      connected: false,
    });
    tick(100);
    expect(isOnlineSpy).toHaveBeenCalled();
    expect(component.errorMessage.title).toBe('Connexion internet');
  }));
  */
});
