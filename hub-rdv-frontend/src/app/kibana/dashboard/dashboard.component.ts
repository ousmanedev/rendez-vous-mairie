import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

enum KibanaDashName {
  Metier = 'metier',
  Recherches = 'recherche',
  TableauDeBordRdvPasseport = 'tableau-de-bord-rdv-passeport',
  Visiteur = 'visiteur',
}
@Component({
  selector: 'rdv-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  @ViewChild('frame') pdfTable?: ElementRef;

  kibanaDashboardName?: string;
  constructor(private sanitizer: DomSanitizer) {}

  ngOnInit(): void {
    this.kibanaDashboardName = window.location.pathname.split('/')[2];
  }

  isKibanaDashName(value: string) {
    return Object.values<string>(KibanaDashName).includes(value);
  }

  public downloadAsPDF() {
    window.print();
  }
}
