import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KibanaRoutingModule } from './kibana-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
  declarations: [DashboardComponent],
  imports: [CommonModule, KibanaRoutingModule],
})
export class KibanaModule {}
