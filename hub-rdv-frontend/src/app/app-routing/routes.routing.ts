import { Routes } from '@angular/router';
import { NotFoundComponent } from '@shared/components/not-found/not-found.component';
import { RouteName } from '@shared/enums/route-name';

export const routes: Routes = [
  {
    path: RouteName.Home,
    loadChildren: () => import('../portal-appointment/portal-appointment.module').then((module) => module.PortalAppointmentModule),
  },
  {
    path: RouteName.Predemande,
    loadChildren: () => import('../predemande/predemande.module').then((module) => module.PredemandeModule),
  },
  {
    path: RouteName.Core,
    loadChildren: () => import('../core/core.module').then((module) => module.CoreModule),
  },
  {
    path: RouteName.Kibana,
    loadChildren: () => import('../kibana/kibana.module').then((module) => module.KibanaModule),
  },
  { path: '**', component: NotFoundComponent },
];
