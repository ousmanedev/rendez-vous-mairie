import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { BreadcrumbItem } from '@shared/classes/BreadcrumbItem';

@Component({
  selector: 'rdv-donnees-personnelles',
  templateUrl: './donnees-personnelles.component.html',
  styleUrls: ['./donnees-personnelles.component.scss'],
})
export class DonneesPersonnellesComponent implements OnInit {
  breadcrumbs?: Array<BreadcrumbItem>;
  constructor(private title: Title) {}

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.title.setTitle('ANTS - Données personnelles');
    this.breadcrumbs = [
      {
        label: 'Accueil',
        url: '/',
      },
      {
        label: 'Données personnelles',
        url: '/donnees-personnelles',
      },
    ];
  }
}
