import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccessibilityComponent } from './accessibility/accessibility.component';
import { CguMentionLegalComponent } from './cgu-mention-legal/cgu-mention-legal.component';
import { DeterritorialisationComponent } from './deterritorialisation/deterritorialisation.component';
import { DonneesPersonnellesComponent } from './donnees-personnelles/donnees-personnelles.component';
import { StatusComponent } from './status/status.component';
import { InformationsComponent } from './informations/informations.component';
import { PreDemandeEnLigneComponent } from './pre-demande-en-ligne/pre-demande-en-ligne.component';
import { RejoindreLaDemarcheComponent } from './rejoindre-la-demarche/rejoindre-la-demarche.component';
import { PlanDuSiteComponent } from './plan-du-site/plan-du-site.component';

export const routes: Routes = [
  { path: 'CGU-mentions-legales', component: CguMentionLegalComponent },
  { path: 'status', component: StatusComponent },
  { path: 'accessibilite', component: AccessibilityComponent },
  { path: 'deterritorialisation', component: DeterritorialisationComponent },
  { path: 'donnees-personnelles', component: DonneesPersonnellesComponent },
  { path: 'informations', component: InformationsComponent },
  { path: 'pre-demande-en-ligne', component: PreDemandeEnLigneComponent },
  { path: 'rejoindre-la-demarche', component: RejoindreLaDemarcheComponent },
  { path: 'plan-du-site', component: PlanDuSiteComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoreRoutingModule {}
