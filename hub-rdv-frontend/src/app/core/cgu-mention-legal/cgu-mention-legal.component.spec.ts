import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CguMentionLegalComponent } from './cgu-mention-legal.component';
import { Title } from '@angular/platform-browser';

describe('CguMentionLegalComponent', () => {
  let component: CguMentionLegalComponent;
  let fixture: ComponentFixture<CguMentionLegalComponent>;
  let titleServiceSpy: Title;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CguMentionLegalComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CguMentionLegalComponent);
    component = fixture.componentInstance;
    titleServiceSpy = TestBed.inject(Title);
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should setTitle Accessibilité when ngOnInit', () => {
    spyOn(titleServiceSpy, 'setTitle');
    component.ngOnInit();
    expect(titleServiceSpy.setTitle).toHaveBeenCalled();
  });
});
