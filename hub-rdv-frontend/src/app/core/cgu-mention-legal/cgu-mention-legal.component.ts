import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { BreadcrumbItem } from '@shared/classes/BreadcrumbItem';

@Component({
  selector: 'rdv-cgu-mention-legal',
  templateUrl: './cgu-mention-legal.component.html',
  styleUrls: ['./cgu-mention-legal.component.scss'],
})
export class CguMentionLegalComponent implements OnInit {
  breadcrumbs?: Array<BreadcrumbItem>;

  constructor(private titleService: Title) {}

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.breadcrumbs = [
      {
        label: 'Accueil',
        url: '/',
      },
      {
        label: 'CGU et mentions légales',
        url: '/CGU-mentions-legales',
      },
    ];
    this.titleService.setTitle("ANTS - Conditions Générales d'Utilisation (CGU)");
  }
}
