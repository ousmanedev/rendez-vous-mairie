import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessibilityComponent } from './accessibility.component';
import { Title } from '@angular/platform-browser';

describe('AccessibilityComponent', () => {
  let component: AccessibilityComponent;
  let fixture: ComponentFixture<AccessibilityComponent>;
  let titleServiceSpy: Title;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AccessibilityComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(AccessibilityComponent);
    component = fixture.componentInstance;
    titleServiceSpy = TestBed.inject(Title);
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should setTitle Accessibilité when ngOnInit', () => {
    spyOn(titleServiceSpy, 'setTitle');
    component.ngOnInit();
    expect(titleServiceSpy.setTitle).toHaveBeenCalled();
  });
});
