import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeterritorialisationComponent } from './deterritorialisation.component';

describe('DeterritorialisationComponent', () => {
  let component: DeterritorialisationComponent;
  let fixture: ComponentFixture<DeterritorialisationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DeterritorialisationComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DeterritorialisationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
