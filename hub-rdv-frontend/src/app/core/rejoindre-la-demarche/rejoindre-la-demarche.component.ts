import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { BreadcrumbItem } from '@shared/classes/BreadcrumbItem';

@Component({
  selector: 'rdv-rejoindre-la-demarche',
  templateUrl: './rejoindre-la-demarche.component.html',
  styleUrls: ['./rejoindre-la-demarche.component.scss'],
})
export class RejoindreLaDemarcheComponent implements OnInit {
  breadcrumbs?: Array<BreadcrumbItem>;
  constructor(private title: Title) {}

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.title.setTitle('ANTS - Rejoindre la démarche');
    this.breadcrumbs = [
      {
        label: 'Accueil',
        url: '/',
      },
      {
        label: 'Rejoindre la démarche',
        url: '/rejoindre-la-demarche',
      },
    ];
  }
}
