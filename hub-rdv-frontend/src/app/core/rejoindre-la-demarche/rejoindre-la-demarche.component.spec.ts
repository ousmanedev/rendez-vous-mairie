import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RejoindreLaDemarcheComponent } from './rejoindre-la-demarche.component';

describe('RejoindreLaDemarcheComponent', () => {
  let component: RejoindreLaDemarcheComponent;
  let fixture: ComponentFixture<RejoindreLaDemarcheComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RejoindreLaDemarcheComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(RejoindreLaDemarcheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
