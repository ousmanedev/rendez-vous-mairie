import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { BreadcrumbItem } from '@shared/classes/BreadcrumbItem';

@Component({
  selector: 'rdv-pre-demande-en-ligne',
  templateUrl: './pre-demande-en-ligne.component.html',
  styleUrls: ['./pre-demande-en-ligne.component.scss'],
})
export class PreDemandeEnLigneComponent implements OnInit {
  breadcrumbs?: Array<BreadcrumbItem>;
  constructor(private title: Title) {}

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.title.setTitle('ANTS - La pré-demande en ligne');
    this.breadcrumbs = [
      {
        label: 'Accueil',
        url: '/',
      },
      {
        label: 'La pré-demande en ligne',
        url: '/pre-demande-en-ligne',
      },
    ];
  }
}
