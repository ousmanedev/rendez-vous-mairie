import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreDemandeEnLigneComponent } from './pre-demande-en-ligne.component';

describe('PreDemandeEnLigneComponent', () => {
  let component: PreDemandeEnLigneComponent;
  let fixture: ComponentFixture<PreDemandeEnLigneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PreDemandeEnLigneComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PreDemandeEnLigneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
