import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { BreadcrumbItem } from '@shared/classes/BreadcrumbItem';

@Component({
  selector: 'rdv-plan-du-site',
  templateUrl: './plan-du-site.component.html',
  styleUrls: ['./plan-du-site.component.scss'],
})
export class PlanDuSiteComponent implements OnInit {
  breadcrumbs?: Array<BreadcrumbItem>;
  constructor(private title: Title) {}

  ngOnInit(): void {
    window.scrollTo(0, 0);
    this.title.setTitle('ANTS - Informations sur la déterritorialisation');
    this.breadcrumbs = [
      {
        label: 'Accueil',
        url: '/',
      },
      {
        label: 'Plan du site',
        url: '/plan-du-site',
      },
    ];
  }
}
