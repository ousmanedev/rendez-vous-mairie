import { NgModule, SecurityContext } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { CoreRoutingModule } from './core-routing.module';
import { MarkdownModule } from 'ngx-markdown';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CguMentionLegalComponent } from './cgu-mention-legal/cgu-mention-legal.component';
import { AccessibilityComponent } from './accessibility/accessibility.component';
import { StatusComponent } from './status/status.component';
import { DonneesPersonnellesComponent } from './donnees-personnelles/donnees-personnelles.component';
import { DeterritorialisationComponent } from './deterritorialisation/deterritorialisation.component';
import { SharedModule } from '@shared/shared.module';
import { PreDemandeEnLigneComponent } from './pre-demande-en-ligne/pre-demande-en-ligne.component';
import { RejoindreLaDemarcheComponent } from './rejoindre-la-demarche/rejoindre-la-demarche.component';
import { InformationsComponent } from './informations/informations.component';
import { PlanDuSiteComponent } from './plan-du-site/plan-du-site.component';

@NgModule({
  declarations: [
    CguMentionLegalComponent,
    AccessibilityComponent,
    StatusComponent,
    DonneesPersonnellesComponent,
    DeterritorialisationComponent,
    PreDemandeEnLigneComponent,
    RejoindreLaDemarcheComponent,
    InformationsComponent,
    PlanDuSiteComponent,
  ],
  imports: [
    CoreRoutingModule,
    CommonModule,
    HttpClientModule,
    MarkdownModule.forRoot({ loader: HttpClient, sanitize: SecurityContext.NONE }),
    NgOptimizedImage,
    SharedModule,
  ],
})
export class CoreModule {}
