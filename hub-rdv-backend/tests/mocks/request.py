from fastapi import Request

requestMock = Request(
    {
        "type": "http",
        "path": "/",
        "client": {"host": "127.0.0.1", "port": "80"},
        "headers": {},
    }
)
