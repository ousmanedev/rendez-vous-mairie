from fastapi.testclient import TestClient
from starlette.status import HTTP_200_OK

from tests.mocks import search_criteria as sc


def test_meeting_points_from_position(client: TestClient):
    response = client.get("/api/MeetingPointsFromPosition", params=sc.search_criteria)
    assert response.status_code == HTTP_200_OK


def test_search_city(client: TestClient):
    response = client.get("/api/searchCity", params={"name": "paris"})
    assert response.status_code == HTTP_200_OK
