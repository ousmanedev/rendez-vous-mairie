import json

from fastapi.testclient import TestClient
from starlette.status import (
    HTTP_200_OK,
    HTTP_401_UNAUTHORIZED,
    HTTP_422_UNPROCESSABLE_ENTITY,
)

from src.hubrdvmairie.db.utils import set_all_editors, set_all_meeting_points
from tests.mocks.editor import test_editor
from tests.mocks.meeting_point import list_meeting_points
from tests.mocks.time_slot import list_time_slots


def test_get_managed_meeting_points_ok(mocker, client: TestClient):
    set_all_meeting_points(list_meeting_points)
    mocker.patch("os.environ.get", return_value=json.dumps(["test-token"]))
    headers = {"x-hub-rdv-auth-token": "test-token"}
    response = client.get("/api/getManagedMeetingPoints", headers=headers)
    assert response.status_code == HTTP_200_OK


def test_get_managed_meeting_points_unauthorized(mocker, client: TestClient):
    mocker.patch("os.environ.get", return_value=json.dumps(["test-token"]))
    headers = {"x-hub-rdv-auth-token": "wrong-token", "origin": "some-unknown-origin"}
    response = client.get("/api/getManagedMeetingPoints", headers=headers)
    assert response.status_code == HTTP_401_UNAUTHORIZED


def test_get_available_time_slots_unauthorized_ok(mocker, client: TestClient):
    set_all_editors([test_editor])
    set_all_meeting_points(list_meeting_points)
    mocker.patch("os.environ.get", return_value=json.dumps(["test-token"]))
    mocker.patch(
        "src.hubrdvmairie.models.editor.Editor.get_available_time_slots",
        return_value=({list_meeting_points[0]["_internal_id"]: list_time_slots}, None),
    )
    headers = {"x-hub-rdv-auth-token": "test-token"}
    parameters = {
        "meeting_point_ids": [list_meeting_points[0]["id"]],
        "start_date": "2022-12-01",
        "end_date": "2022-12-31",
    }
    response = client.get("/api/availableTimeSlots", headers=headers, params=parameters)
    assert response.status_code == HTTP_200_OK
    json_repsonse = response.json()
    assert (list_meeting_points[0]["id"] in json_repsonse) and len(
        json_repsonse[list_meeting_points[0]["id"]]
    ) > 0


def test_get_available_time_slots_unauthorized_ok_empty(mocker, client: TestClient):
    set_all_editors([test_editor])
    set_all_meeting_points(list_meeting_points)
    mocker.patch("os.environ.get", return_value=json.dumps(["test-token"]))
    mocker.patch(
        "src.hubrdvmairie.models.editor.Editor.get_available_time_slots",
        return_value=({list_meeting_points[0]["_internal_id"]: []}, None),
    )
    headers = {"x-hub-rdv-auth-token": "test-token"}
    parameters = {
        "meeting_point_ids": [list_meeting_points[0]["id"]],
        "start_date": "2022-12-01",
        "end_date": "2022-12-31",
    }
    response = client.get("/api/availableTimeSlots", headers=headers, params=parameters)
    assert response.status_code == HTTP_200_OK
    json_repsonse = response.json()
    assert (list_meeting_points[0]["id"] not in json_repsonse) or not json_repsonse[
        list_meeting_points[0]["id"]
    ]


def test_get_available_time_slots_unauthorized(mocker, client: TestClient):
    mocker.patch("os.environ.get", return_value=json.dumps(["test-token"]))
    headers = {"x-hub-rdv-auth-token": "wrong-token", "origin": "some-unknown-origin"}
    response = client.get("/api/availableTimeSlots", headers=headers)
    assert response.status_code == HTTP_401_UNAUTHORIZED


def test_search_application_ids_ok(mocker, client: TestClient):
    pre_application_id = "G12DZE123M"
    mocker.patch("os.environ.get", return_value=json.dumps(["test-token"]))
    mocker.patch(
        "src.hubrdvmairie.controllers.routes.external.verify_recaptcha",
        return_value=0.51,
    )
    mocker.patch(
        "src.hubrdvmairie.models.editor.Editor.search_meetings",
        return_value={
            pre_application_id: [
                {
                    "meeting_point": "Mairie ANNEXE LILLE-SECLIN",
                    "datetime": "2022-12-19T10:00Z",
                    "management_url": "http://www.ville-seclin.fr/rendez-vous/predemande?num=6123155111",
                    "cancel_url": "http://www.ville-seclin.fr/rendez-vous/annulation?num=6123155111",
                }
            ]
        },
    )

    headers = {"x-hub-rdv-auth-token": "test-token", "recaptcha_token": "testToken"}
    parameters = {"application_ids": [pre_application_id]}
    client.get("/api/searchApplicationIds", headers=headers, params=parameters)
    response = client.get(
        "/api/searchApplicationIds", headers=headers, params=parameters
    )
    assert response.status_code == HTTP_200_OK


def test_search_application_ids_unauthorized(mocker, client: TestClient):
    mocker.patch("os.environ.get", return_value=json.dumps(["test-token"]))
    headers = {
        "x-hub-rdv-auth-token": "wrong-token-token",
        "origin": "some-unknown-origin",
    }
    parameters = {"application_ids": ["1111111111"]}
    response = client.get(
        "/api/searchApplicationIds", headers=headers, params=parameters
    )
    assert response.status_code == HTTP_401_UNAUTHORIZED


def test_search_application_ids_bad_param(mocker, client: TestClient):
    pre_application_id = "123"
    mocker.patch("os.environ.get", return_value=json.dumps(["test-token"]))
    headers = {"x-hub-rdv-auth-token": "test-token"}
    parameters = {"application_ids": [pre_application_id]}
    response = client.get(
        "/api/searchApplicationIds", headers=headers, params=parameters
    )
    assert response.status_code == HTTP_422_UNPROCESSABLE_ENTITY
