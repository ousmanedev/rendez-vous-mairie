from datetime import date, timedelta

import pytest
from pytest_mock import MockerFixture

from src.hubrdvmairie.controllers.routes.external import (
    get_available_time_slots,
    search_application_ids,
)
from src.hubrdvmairie.db.utils import set_all_editors, set_all_meeting_points
from tests.mocks.editor import test_editor
from tests.mocks.meeting_point import list_meeting_points
from tests.mocks.request import requestMock
from tests.mocks.time_slot import list_time_slots


@pytest.mark.asyncio
async def test_get_available_time_slots_ok(mocker: MockerFixture) -> None:
    """
    test the route method get_available_time_slots if we use correct parameters
    """
    set_all_editors([test_editor])
    set_all_meeting_points(list_meeting_points)
    mocker.patch(
        "src.hubrdvmairie.models.editor.Editor.get_available_time_slots",
        return_value=({list_meeting_points[0]["_internal_id"]: list_time_slots}, None),
    )
    result = await get_available_time_slots(
        requestMock,
        [list_meeting_points[0]["id"], list_meeting_points[1]["id"]],
        start_date=date.today(),
        end_date=date.today() + timedelta(days=90),
    )
    assert isinstance(result, dict)
    assert list_meeting_points[0]["id"] in result
    assert list_meeting_points[1]["id"] not in result
    assert len(result[list_meeting_points[0]["id"]]) > 0


@pytest.mark.asyncio
async def test_get_available_time_slots_ok_empty(mocker: MockerFixture) -> None:
    """
    test the route method get_available_time_slots if we use correct parameters
    """
    set_all_editors([test_editor])
    set_all_meeting_points(list_meeting_points)
    mocker.patch(
        "src.hubrdvmairie.models.editor.Editor.get_available_time_slots",
        return_value=({list_meeting_points[0]["_internal_id"]: []}, None),
    )
    result = await get_available_time_slots(
        requestMock,
        [list_meeting_points[0]["id"], list_meeting_points[1]["id"]],
        start_date=date.today(),
        end_date=date.today() + timedelta(days=90),
    )
    assert isinstance(result, dict)
    assert list_meeting_points[0]["id"] in result
    assert list_meeting_points[1]["id"] not in result
    assert len(result[list_meeting_points[0]["id"]]) == 0


@pytest.mark.asyncio
async def test_search_application_ids_ok(mocker):
    pre_application_id = "G12DZE123M"
    mocker.patch(
        "src.hubrdvmairie.models.editor.Editor.search_meetings",
        return_value={
            pre_application_id: [
                {
                    "meeting_point": "Mairie ANNEXE LILLE-SECLIN",
                    "datetime": "2022-12-19T10:00Z",
                    "management_url": "http://www.ville-seclin.fr/rendez-vous/predemande?num=6123155111",
                    "cancel_url": "http://www.ville-seclin.fr/rendez-vous/annulation?num=6123155111",
                }
            ]
        },
    )
    mocker.patch(
        "src.hubrdvmairie.controllers.routes.external.verify_recaptcha",
        return_value=0.51,
    )
    result = await search_application_ids(
        request=requestMock, application_ids=[pre_application_id]
    )
    assert isinstance(result, dict)
    assert pre_application_id in result and len(result[pre_application_id]) > 0


@pytest.mark.asyncio
async def test_search_application_ids_ok_empty(mocker):
    pre_application_id = "G12DZE123M"
    mocker.patch(
        "src.hubrdvmairie.models.editor.Editor.search_meetings",
        return_value={pre_application_id: []},
    )
    mocker.patch(
        "src.hubrdvmairie.controllers.routes.external.search_appointments_in_optimisation_api",
        return_value={pre_application_id: []},
    )
    mocker.patch(
        "src.hubrdvmairie.controllers.routes.external.verify_recaptcha",
        return_value=0.51,
    )
    result = await search_application_ids(
        request=requestMock, application_ids=[pre_application_id]
    )
    assert isinstance(result, dict)
    assert (pre_application_id not in result) or len(result[pre_application_id]) == 0
