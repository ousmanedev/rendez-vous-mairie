"""This is the main script that initiate the api and load the variable
environment and initiate the logger and execute a scheduled jobs.
"""
import json
import logging
import os
from datetime import datetime

import uvicorn
from dotenv import load_dotenv
from fastapi import FastAPI, Request, Response
from fastapi.middleware.cors import CORSMiddleware
from fastapi_utils.tasks import repeat_every
from Secweb.ContentSecurityPolicy import ContentSecurityPolicy
from Secweb.OriginAgentCluster import OriginAgentCluster
from Secweb.ReferrerPolicy import ReferrerPolicy
from Secweb.XContentTypeOptions import XContentTypeOptions
from Secweb.XFrameOptions import XFrame
from Secweb.xXSSProtection import xXSSProtection
from starlette.background import BackgroundTask

from _version import __version__
from src.hubrdvmairie.controllers.controller import router as api_router
from src.hubrdvmairie.core.config import get_settings
from src.hubrdvmairie.db.utils import reset_all_ws_use_rates, set_all_editors
from src.hubrdvmairie.logging.app_logger import get_logger, write_access_log_data
from src.hubrdvmairie.logging.app_logger_formatter import CustomFormatter
from src.hubrdvmairie.models.editor import init_all_editors
from src.hubrdvmairie.services.scheduled_functions import run_daily_scheduled_functions

logging.getLogger("uvicorn").handlers.clear()
logging.getLogger("uvicorn").propagate = False

load_dotenv()


async def catch_exceptions_middleware(request: Request, call_next):
    try:
        return await call_next(request)
    except Exception:
        try:
            return Response("Internal server error", status_code=500)
        except Exception as double_e:
            # Err non remontée à l'usager
            # Print pour investigation
            print("Erreur, 500 impossible : ", double_e.__cause__)


def get_application() -> FastAPI:
    """Initiate and configure the api

    Returns:
        FastAPI: Object that lunch our api server
    """
    application = FastAPI(title=get_settings().project_name, version=__version__)
    application.middleware("http")(catch_exceptions_middleware)
    allow_origins = ["*"]
    if os.environ.get("ALLOW_ORIGINS"):
        allow_origins = json.loads(os.environ.get("ALLOW_ORIGINS"))
    application.add_middleware(
        CORSMiddleware,
        allow_origins=allow_origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    application.include_router(api_router, prefix=get_settings().api_prefix)
    application.add_middleware(
        ContentSecurityPolicy,
        Option={
            "default-src": ["'self'"],
            "base-uri": ["'self'"],
            "block-all-mixed-content": [],
            "font-src": ["'self'", "https:", "data:"],
            "frame-ancestors": ["'self'"],
            "img-src": ["'self'", "data:", "https://fastapi.tiangolo.com"],
            "object-src": ["'none'"],
            "script-src": ["'self'", "https://cdn.jsdelivr.net", "'unsafe-inline'"],
            "script-src-attr": ["'none'"],
            "style-src": ["'self'", "https:", "'unsafe-inline'"],
            "upgrade-insecure-requests": [],
            # "require-trusted-types-for": ["'script'"],
        },
    )
    application.add_middleware(OriginAgentCluster)
    application.add_middleware(ReferrerPolicy)
    application.add_middleware(XContentTypeOptions)
    application.add_middleware(xXSSProtection, Option={"X-XSS-Protection": "0"})
    application.add_middleware(XFrame, Option={"X-Frame-Options": "DENY"})
    return application


app = get_application()


formatter = CustomFormatter("%(asctime)s")
logger = get_logger("root", formatter)
logger.setLevel(os.environ.get("LOG_LEVEL") or logging.INFO)


@app.middleware("http")
async def log_request(request: Request, call_next):
    start_time = datetime.now()
    response = await call_next(request)
    response_time = (datetime.now() - start_time).microseconds / 1000
    response.response_time = response_time
    response.background = BackgroundTask(
        write_access_log_data, logger, request, response
    )
    return response


# init the list of editors
set_all_editors(init_all_editors())


@app.on_event("startup")
@repeat_every(seconds=60 * 60 * 24)
async def daily_scheduled_functions():
    await run_daily_scheduled_functions()


@app.on_event("startup")
@repeat_every(seconds=60)
def reset_ws_use_rates():
    """function that intiate ws_use_rates to empty object"""
    reset_all_ws_use_rates()


if __name__ == "__main__":
    logger.info("Server started listening on port: %s", os.environ.get("PORT"))
    uvicorn.run(
        "main:app",
        host=os.environ.get("HOST"),
        log_level=logging.ERROR,
        port=int(os.environ.get("PORT")),
        reload=os.environ.get("UVICORN_RELOAD") in [True, "True"],
        reload_includes=["hub-rdv-backend"],
        reload_excludes=[".pytest_cache", "hub-rdv-backend/.pytest_cache"],
        workers=int(os.environ.get("WORKERS")),
    )
